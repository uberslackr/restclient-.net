﻿The Silverlight sample has not been tested for a while. It was designed to use the REST 
service in this repo called CF.REST.Sample. It may or may not be working but Silverlight
has a silly "security" feature where you can only hit services where there is a 
clientaccesspolicy.xml . If you want to test and get this working, please feel free to 
submit a pull request.