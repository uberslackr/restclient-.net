﻿namespace CF.RESTClientDotNet
{
    public enum HttpVerb
    {
        Post,
        Get,
        Put
    }
}
