﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyVersion("0.5.0")]
[assembly: AssemblyFileVersion("0.5.0")]
[assembly: AssemblyCompany("Christian Findlay")]
[assembly: AssemblyProduct("RESTClient.NET")]
[assembly: AssemblyCopyright("Copyright © 2017 Christian Findlay")]
[assembly: CLSCompliant(true)]
[assembly: NeutralResourcesLanguage("en")]
// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
//TODO: Is this necessary? Should each assembly be unique?
//[assembly: Guid("8171457b-9ea1-4893-985b-f241918f57af")]
